/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.event;

import java.util.LinkedList;

/**
 * @author mush
 */
public class DelayedGameEvents {

    private final LinkedList<DelayedEvent> delayedEventList;

    DelayedGameEvents() {
        delayedEventList = new LinkedList<>();
    }

    public void sendEvent(Object event, double delay) {
        DelayedEvent delayedEvent = new DelayedEvent();
        delayedEvent.event = event;
        delayedEvent.remainingSeconds = delay;
        delayedEventList.add(delayedEvent);
    }

    private void sendEventNow(Object category, DelayedEvent delayedEvent) {
        if (category == null) {
            GameEventQueue.global().sendEvent(delayedEvent.event);
        } else {
            GameEventQueue.category(category).sendEvent(delayedEvent.event);
        }
    }

    void update(Object category, double elapsedSeconds) {
        LinkedList<DelayedEvent> oldList = (LinkedList<DelayedEvent>) delayedEventList.clone();
        delayedEventList.clear();

        oldList.forEach((delayedEvent) -> {
            delayedEvent.remainingSeconds -= elapsedSeconds;
            if (delayedEvent.remainingSeconds <= 0) {
                sendEventNow(category, delayedEvent);
            } else {
                delayedEventList.add(delayedEvent);
            }
        });

        oldList.clear();
    }

    private static class DelayedEvent {

        Object event;
        double remainingSeconds;
    }
}
