/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.event;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author mush
 */
public class GameEventQueue {

    private static final SimpleGameEventQueue GLOBAL_INSTANCE;
    private static final ConcurrentMap<Object, SimpleGameEventQueue> CATEGORY_INSTANCES;
    private static final DelayedGameEvents GLOBAL_DELAYED_INSTANCE;
    private static final ConcurrentMap<Object, DelayedGameEvents> CATEGORY_DELAYED_INSTANCES;

    static {
        GLOBAL_INSTANCE = new SimpleGameEventQueue();
        CATEGORY_INSTANCES = new ConcurrentHashMap<>();
        GLOBAL_DELAYED_INSTANCE = new DelayedGameEvents();
        CATEGORY_DELAYED_INSTANCES = new ConcurrentHashMap<>();
    }

    private GameEventQueue() {
    }

    public static SimpleGameEventQueue global() {
        return GLOBAL_INSTANCE;
    }

    public static SimpleGameEventQueue category(Object category) {
        return CATEGORY_INSTANCES.computeIfAbsent(category, (k) -> {
            return new SimpleGameEventQueue();
        });
    }

    public static DelayedGameEvents delayed() {
        return GLOBAL_DELAYED_INSTANCE;
    }

    public static DelayedGameEvents delayed(Object category) {
        return CATEGORY_DELAYED_INSTANCES.computeIfAbsent(category, (k) -> {
            return new DelayedGameEvents();
        });
    }

    @Deprecated
    public static void send(Object event) {
        global().sendEvent(event);
    }

    @Deprecated
    public static void addListener(Object listener) {
        global().addEventListener(listener);
    }

    @Deprecated
    public static void removeListener(Object listener) {
        global().removeEventListener(listener);
    }

    public static void processQueue() {
        GLOBAL_INSTANCE.process();
        CATEGORY_INSTANCES.forEach((k, v) -> {
            v.process();
        });
    }

    public static void updateDelayed(double elapsedSeconds) {
        GLOBAL_DELAYED_INSTANCE.update(null, elapsedSeconds);
        CATEGORY_DELAYED_INSTANCES.forEach((category, v) -> {
            v.update(category, elapsedSeconds);
        });
    }

    public enum Categories {
        INPUT,
        SOUND,
        NAVIGATION,
        SYSTEM
    }

}
