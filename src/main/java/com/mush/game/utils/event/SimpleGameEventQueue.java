/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * @author mush
 */
public class SimpleGameEventQueue {

    private final List<Object> listeners;
    private final List<Object> addListeners;
    private final List<Object> removeListeners;
    private final LinkedList<Object> queue;
    private final Map<Class<?>, Map<Class<?>, Method>> listenerEventMethodMap;

    SimpleGameEventQueue() {
        listeners = new ArrayList<>();
        addListeners = new ArrayList<>();
        removeListeners = new ArrayList<>();
        listenerEventMethodMap = new HashMap<>();
        queue = new LinkedList<>();
    }

    public void sendEvent(Object event) {
        queue.add(event);
    }

    public void addEventListener(Object listener) {
        synchronized (addListeners) {
            addListeners.add(listener);
        }
    }

    public void removeEventListener(Object listener) {
        synchronized (removeListeners) {
            removeListeners.add(listener);
        }
    }

    private void cleanupRemovedListeners() {
        if (removeListeners.isEmpty()) {
            return;
        }

        synchronized (removeListeners) {
            for (Object listener : removeListeners) {
                listeners.remove(listener);
            }

            removeListeners.clear();
        }
    }

    private void addNewListeners() {
        if (addListeners.isEmpty()) {
            return;
        }

        synchronized (addListeners) {
            for (Object listener : addListeners) {
                listeners.add(listener);
                collectOnEventMethods(listener);
            }

            addListeners.clear();
        }
    }

    void process() {
        try {
            cleanupRemovedListeners();

            LinkedList<Object> oldQueue = (LinkedList<Object>) queue.clone();
            queue.clear();

            for (Object event : oldQueue) {
                process(event);
            }
            oldQueue.clear();

            addNewListeners();

        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    private void process(Object event) {
        for (Object listener : listeners) {
            invokeOnEvent(event, listener);
        }
    }

    private void invokeOnEvent(Object event, Object listener) {
        try {
            Method onEvent = getOnEventMethod(event, listener);

            if (onEvent != null) {
                onEvent.invoke(listener, event);
            }

        } catch (SecurityException
                | IllegalAccessException
                | IllegalArgumentException
                | InvocationTargetException ex) {

            ex.printStackTrace(System.out);
        }
    }

    private Method getOnEventMethod(Object event, Object listener) {
        Map<Class<?>, Method> eventMethodMap = listenerEventMethodMap.get(listener.getClass());
        if (eventMethodMap == null) {
            return null;
        }
        return eventMethodMap.get(event.getClass());
    }

    private void collectOnEventMethods(Object listener) {
        Class<?> listenerClass = listener.getClass();

        try {
            Method[] methods = listenerClass.getMethods();
            for (Method method : methods) {
                checkListenerMethod(listenerClass, method);
            }

        } catch (SecurityException | IllegalArgumentException ex) {
            //
        }
    }

    private void checkListenerMethod(Class<?> listenerClass, Method method) {
        OnGameEvent annotation = method.getAnnotation(OnGameEvent.class);
        if (annotation != null) {
            if (method.getParameterCount() == 1) {
                Class<?>[] params = method.getParameterTypes();
                Class<?> paramClass = params[0];
                addListenerEventMethod(listenerClass, paramClass, method);
            }
        }
    }

    private void addListenerEventMethod(Class<?> listenerClass, Class<?> eventClass, Method method) {
        listenerEventMethodMap
                .computeIfAbsent(listenerClass, k -> new HashMap<>())
                .put(eventClass, method);
    }

}
