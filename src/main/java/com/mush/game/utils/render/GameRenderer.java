package com.mush.game.utils.render;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.mush.game.utils.core.GameDebug.gameDebug;

public class GameRenderer {

    private final Logger logger = Logger.getLogger(this.getClass().getSimpleName());
    private final int fpsHistoryLength = 50;
    private final double[] fpsHistory = new double[fpsHistoryLength];
    private final Map<RenderingHints.Key, Object> renderingHintsMap;
    private BufferedImage screenImage;
    private Graphics2D screenGraphics;
    private BufferedImage bufferImage;
    private Graphics2D bufferGraphics;
    private int screenWidth;
    private int screenHeight;
    private double pixelScale = 1;
    private double fps;
    private int fpsHistoryIndex = 0;
    private boolean useBuffer = false;
    private int contentOffsetX = 0;
    private int contentOffsetY = 0;
    private Color backgroundColor = Color.BLACK;
    private Renderable renderable;

    public GameRenderer(int width, int height) {
        renderingHintsMap = new HashMap<>();
        setScreenSize(width, height);
    }

    public GameRenderer(int width, int height, int scale) {
        this.screenWidth = width;
        this.screenHeight = height;
        this.pixelScale = scale;
        renderingHintsMap = new HashMap<>();

        createScreenImage();
    }

    public GameRenderer setRenderable(Renderable renderable) {
        this.renderable = renderable;
        return this;
    }

    public BufferedImage getScreenImage() {
        return screenImage;
    }

    public double getFps() {
        return fps;
    }

    public void setContentOffset(int x, int y) {
        contentOffsetX = x;
        contentOffsetY = y;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public final void setScreenSize(int width, int height) {
        this.screenWidth = width;
        this.screenHeight = height;

        createScreenImage();
    }

    public final void setPixelScale(double scale) {
        this.pixelScale = scale;
    }

    public final void setUseBuffer(boolean v) {
        useBuffer = true;
        createScreenImage();
    }

    private void createScreenImage() {
        if (screenGraphics != null) {
            screenGraphics.dispose();
            screenGraphics = null;
            screenImage = null;
        }

        screenImage = new BufferedImage(
                (int) Math.ceil(screenWidth / pixelScale),
                (int) Math.ceil(screenHeight / pixelScale),
                BufferedImage.TYPE_INT_RGB);

        screenGraphics = screenImage.createGraphics();

        if (useBuffer) {
            createBufferImage();
        } else {
            destroyBufferImage();
        }

        applyRenderingHints();

        logger.log(Level.INFO, "Screen size: {0} {1}", new Object[]{screenWidth, screenHeight});
        logger.log(Level.INFO, "Image size: {0} {1}", new Object[]{screenImage.getWidth(), screenImage.getHeight()});
    }

    private void destroyBufferImage() {
        if (bufferGraphics != null) {
            bufferGraphics.dispose();
            bufferGraphics = null;
            bufferImage = null;
        }
    }

    private void createBufferImage() {
        destroyBufferImage();

        bufferImage = new BufferedImage(
                screenImage.getWidth(),
                screenImage.getHeight(),
                BufferedImage.TYPE_INT_RGB);

        bufferGraphics = bufferImage.createGraphics();
    }

    private void swapBuffer() {
        if (bufferImage == null) {
            return;
        }

        BufferedImage tempImage = screenImage;
        Graphics2D tempGraphics = screenGraphics;

        screenImage = bufferImage;
        screenGraphics = bufferGraphics;

        bufferImage = tempImage;
        bufferGraphics = tempGraphics;
    }

    public void setRenderingHint(RenderingHints.Key hintKey, Object hintValue) {
        renderingHintsMap.put(hintKey, hintValue);
    }

    public void applyRenderingHints() {
        for (Map.Entry<RenderingHints.Key, Object> hint : renderingHintsMap.entrySet()) {
            screenGraphics.setRenderingHint(hint.getKey(), hint.getValue());
        }
    }

    public final void render() {
        if (useBuffer) {
            if (renderable != null) {
                renderable.renderContent(bufferGraphics, this);
            }
            swapBuffer();
        } else {
            if (renderable != null) {
                renderable.renderContent(screenGraphics, this);
            }
        }
    }

    private void drawBackground(Graphics2D g) {
        g.setColor(backgroundColor);
        int width = (int) (screenWidth * pixelScale);
        int height = (int) (screenHeight * pixelScale);
        g.fillRect(0, 0, contentOffsetX, height);
        g.fillRect(contentOffsetX + width, 0, contentOffsetX, height);
        g.fillRect(0, 0, width, contentOffsetY);
        g.fillRect(0, contentOffsetY + height, width, contentOffsetY);
    }

    public final void draw(Graphics2D g) {
        AffineTransform t0 = g.getTransform();
        drawBackground(g);
        g.translate(contentOffsetX, contentOffsetY);
        AffineTransform t = null;
        if (pixelScale > 1) {
            t = g.getTransform();
            g.scale(pixelScale, pixelScale);
        }
        g.drawImage(screenImage, 0, 0, null);

        if (t != null) {
            g.setTransform(t);
        }

        if (gameDebug().showFps) {
            if (gameDebug().showFpsHistory) {
                drawFpsHistory(g);
            } else {
                drawFps(g, fps);
            }
        }
        g.setTransform(t0);
    }

    public int getScreenWidth() {
        return screenWidth;
    }

    public int getScreenHeight() {
        return screenHeight;
    }

    public void updateFps(double fps) {
        this.fps = fps;
        this.fpsHistory[this.fpsHistoryIndex] = fps;
        this.fpsHistoryIndex = (this.fpsHistoryIndex + 1) % this.fpsHistoryLength;
    }

    private void drawFpsHistory(Graphics2D g) {
        double min = 100000;
        double max = 0;
        double total = 0;
        for (double sample : fpsHistory) {
            if (sample < min) {
                min = sample;
            }
            if (sample > max) {
                max = sample;
            }
            total += sample;
        }
        double avg = total / fpsHistoryLength;
        double range = max - min;
        if (range < 1) {
            range = 1;
        }

        int height = 10;
        int x0 = 0;
        int y0 = 30 + height;

        g.setPaint(Color.GREEN);
        for (int i = 0; i < fpsHistoryLength; i++) {
            double v = height * (fpsHistory[i] - min) / range;
            g.drawLine(x0 + i, y0, x0 + i, (int) (y0 - v));
        }

        g.drawString("" + Math.round(min), x0, y0 + 13);
        g.drawString("" + Math.round(max), x0, y0 - 13);

        drawFps(g, avg);
    }

    private void drawFps(Graphics2D g, double v) {
        g.setPaint(Color.BLACK);
        g.drawString("" + Math.round(v), 1, 13);
        g.setPaint(Color.GREEN);
        g.drawString("" + Math.round(v), 0, 12);
    }
}
