package com.mush.game.utils.render;

import java.awt.*;

public interface Renderable {

    void renderContent(Graphics2D g, GameRenderer renderer);
}
