package com.mush.game.utils.sound;

public class GameSoundEvent {
    public final Integer soundId;

    private GameSoundEvent(Integer soundId) {
        this.soundId = soundId;
    }

    public static class SoundStarted extends GameSoundEvent {
        public SoundStarted(Integer soundId) {
            super(soundId);
        }
    }

    public static class SoundFinished extends GameSoundEvent {
        public SoundFinished(Integer soundId) {
            super(soundId);
        }
    }
}
