package com.mush.game.utils.sound;

import org.lwjgl.openal.AL10;

public class SoundSource {

    private final Integer sourceId;
    private Integer soundId;
    private Integer bufferId;
    private State state = State.IDLE;

    public SoundSource(Integer sourceId) {
        this.sourceId = sourceId;
//        sourceId = AL10.alGenSources();
    }

    public void cleanup() {
        AL10.alDeleteSources(sourceId);
        state = State.REMOVED;
    }

    public Integer getSourceId() {
        return sourceId;
    }

    public Integer getBufferId() {
        return bufferId;
    }

    public Integer getSoundId() {
        return soundId;
    }

    public void setSoundId(Integer soundId) {
        this.soundId = soundId;
    }

    public State getState() {
        return state;
    }

    public void play(Integer bufferId) {
        if (state != State.IDLE) {
            return;
        }
        AL10.alSourcei(sourceId, AL10.AL_BUFFER, bufferId);
        AL10.alSourcePlay(sourceId);
        this.bufferId = bufferId;
        state = State.PLAYING;
        setVolume(1.0f);
        setPitch(1.0f);
        setPosition(0.0f);
        setLoop(false);
    }

    public void modify(SoundParams params) {
        if (params.getVolume() != null) {
            setVolume(params.getVolume());
        }
        if (params.getPitch() != null) {
            setPitch(params.getPitch());
        }
        if (params.getPosition() != null) {
            setPosition(params.getPosition());
        }
        if (params.getLoop() != null) {
            setLoop(params.getLoop());
        }
    }

    public void setVolume(float volume) {
        AL10.alSourcef(sourceId, AL10.AL_GAIN, volume);
    }

    private void setPitch(float pitch) {
        AL10.alSourcef(sourceId, AL10.AL_PITCH, pitch);
    }

    private void setPosition(float position) {
        AL10.alSource3f(sourceId, AL10.AL_POSITION, position, 1, 0);
    }

    private void setLoop(boolean loop) {
        AL10.alSourcei(sourceId, AL10.AL_LOOPING, loop ? AL10.AL_TRUE : AL10.AL_FALSE);
    }

    public void pause() {
        AL10.alSourcePause(sourceId);
    }

    public void resume() {
        AL10.alSourcePlay(sourceId);
    }

    public void update() {
        if (!isPlaying() && state == State.PLAYING) {
            state = State.IDLE;
        }
    }

    public boolean isPlaying() {
        return AL10.alGetSourcei(sourceId, AL10.AL_SOURCE_STATE) == AL10.AL_PLAYING;
    }

    public enum State {
        IDLE,
        PLAYING,
        REMOVED
    }
}
