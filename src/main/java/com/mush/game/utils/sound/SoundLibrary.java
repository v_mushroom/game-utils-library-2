package com.mush.game.utils.sound;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SoundLibrary {

    private final Logger logger = Logger.getLogger(this.getClass().getSimpleName());
    private final Map<String, Integer> buffersByName = new HashMap<>();
    private final OpenALFunctions functions;

    public SoundLibrary(OpenALFunctions functions) {
        this.functions = functions;
    }

    public boolean loadSound(String soundName, String fileName) {
        if (buffersByName.containsKey(soundName)) {
            logger.log(Level.INFO, "Sound already loaded {0}", fileName);
            return true;
        }
        logger.log(Level.INFO, "Loading sound [{0}] from file [{1}]", new Object[]{soundName, fileName});
        try {
            Integer bufferId = functions.loadOggSound(fileName);
            if (bufferId == null) {
                logger.log(Level.INFO, "Failed to load sound file [{0}]", new Object[]{fileName});
                return false;
            }
            buffersByName.put(soundName, bufferId);
            return true;
        } catch (Throwable e) {
            logger.log(Level.WARNING, "", e);
            return false;
        }
    }

    public Integer getSoundBufferId(String soundName) {
        return buffersByName.get(soundName);
    }

    public String getSoundNameByBufferId(Integer bufferId) {
        return buffersByName.entrySet().stream()
                .filter(e -> Objects.equals(e.getValue(), bufferId))
                .map(Map.Entry::getKey)
                .findFirst()
                .orElse(null);
    }

    public void cleanup() {
        int[] buffers = buffersByName.values().stream().mapToInt(i -> i).toArray();
        logger.log(Level.INFO, "Cleaning up {0} sound buffers", buffers.length);
        System.out.println("Cleaning up " + buffers.length + " sound buffers");
        try {
            functions.cleanupBuffers(buffers);
        } catch (Throwable e) {
            logger.log(Level.WARNING, "", e);
        }
        buffersByName.clear();
    }
}
