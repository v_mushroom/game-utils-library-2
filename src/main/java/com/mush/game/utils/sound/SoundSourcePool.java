package com.mush.game.utils.sound;

import com.mush.game.utils.event.GameEventQueue;
import org.lwjgl.openal.AL10;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.lwjgl.openal.AL10.alDeleteSources;

public class SoundSourcePool {

    private final Logger logger = Logger.getLogger(this.getClass().getSimpleName());
    private final List<SoundSource> pool = new ArrayList<>();
    private final Map<Integer, SoundSource> sourcesById = new HashMap<>();
    private final AtomicInteger nextSoundId = new AtomicInteger(0);

    public SoundSource prepareSoundSource() {
        SoundSource source = findFreeSoundSource();
        unmap(source);
        setNextId(source);
        map(source);
        return source;
    }

    public Optional<SoundSource> getSoundSourceById(Integer soundId) {
        return Optional.ofNullable(sourcesById.get(soundId));
    }

    public void forAllPlaying(Consumer<SoundSource> consumer) {
        pool.stream()
                .filter(source -> source.getState() == SoundSource.State.PLAYING)
                .forEach(consumer);
    }

    public void update() {
        pool.stream()
                .filter(source -> source.getState() == SoundSource.State.PLAYING)
                .peek(SoundSource::update)
                .filter(source -> source.getState() == SoundSource.State.IDLE)
                .forEach(source -> GameEventQueue
                        .category(GameEventQueue.Categories.SOUND)
                        .sendEvent(new GameSoundEvent.SoundFinished(source.getSoundId())));
    }

    public void cleanup() {
        int[] sourceIds = pool.stream()
                .filter(source -> source.getState() != SoundSource.State.REMOVED)
                .mapToInt(SoundSource::getSourceId)
                .toArray();
        logger.log(Level.INFO, "Cleaning up {0} sound sources", sourceIds.length);
        System.out.println("Cleaning up " + sourceIds.length + " sound sources");
        alDeleteSources(sourceIds);
    }

    private SoundSource findFreeSoundSource() {
        return pool.stream()
                .filter(source -> source.getState() == SoundSource.State.IDLE)
                .findFirst()
                .orElseGet(this::createNew);
    }

    private void unmap(SoundSource source) {
        if (source.getSoundId() != null) {
            sourcesById.remove(source.getSoundId());
            source.setSoundId(null);
        }
    }

    private void setNextId(SoundSource source) {
        source.setSoundId(nextSoundId.addAndGet(1));
    }

    private void map(SoundSource source) {
        sourcesById.put(source.getSoundId(), source);
    }

    private SoundSource createNew() {
        logger.log(Level.FINE, "Creating new sound source");
        SoundSource source = new SoundSource(AL10.alGenSources());
        pool.add(source);
        return source;
    }
}
