package com.mush.game.utils.sound;

import com.mush.game.utils.event.GameEventQueue;

import java.util.logging.Logger;

public class GameAudio {

    private final Logger logger = Logger.getLogger(this.getClass().getSimpleName());
    private final OpenALFunctions functions = new OpenALFunctions();
    private final SoundLibrary library = new SoundLibrary(functions);
    private final SoundSourcePool sourcePool = new SoundSourcePool();

    private boolean initialized = false;
    private boolean cleanedUp = false;

    public GameAudio() {
        init();
    }

    private void init() {
        if (initialized) {
            return;
        }
        logger.info("Initializing sound");
        initialized = functions.init();
    }

    public void cleanup() {
        if (!initialized || cleanedUp) {
            return;
        }
        System.out.println("Cleaning up sound");
        sourcePool.cleanup();
        library.cleanup();
        functions.cleanup();
        cleanedUp = true;
    }

    public boolean loadSound(String soundName, String fileName) {
        return library.loadSound(soundName, fileName);
    }

    public void update(/* elapsed time doesn't really matter here */) {
        sourcePool.update();
    }

    public String getSoundNameForSoundId(Integer soundId) {
        return sourcePool
                .getSoundSourceById(soundId)
                .map(SoundSource::getBufferId)
                .map(library::getSoundNameByBufferId)
                .orElse(null);
    }

    public void modifySound(Integer soundId, SoundParams params) {
        sourcePool
                .getSoundSourceById(soundId)
                .ifPresent(source -> source.modify(params));
    }

    public Integer playSound(String soundName) {
        Integer bufferId = library.getSoundBufferId(soundName);
        if (bufferId == null) {
            return null;
        }
        SoundSource source = sourcePool.prepareSoundSource();
        source.play(bufferId);
        GameEventQueue
                .category(GameEventQueue.Categories.SOUND)
                .sendEvent(new GameSoundEvent.SoundStarted(source.getSoundId()));
        return source.getSoundId();
    }

    public void pause() {
        sourcePool.forAllPlaying(SoundSource::pause);
    }

    public void unpause() {
        sourcePool.forAllPlaying(SoundSource::resume);
    }
}
