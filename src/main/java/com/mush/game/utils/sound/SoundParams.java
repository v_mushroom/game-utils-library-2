package com.mush.game.utils.sound;

public class SoundParams {
    private Float volume;
    private Float position;
    private Float pitch;
    private Boolean loop;

    public Float getVolume() {
        return volume;
    }

    public SoundParams setVolume(Float volume) {
        this.volume = volume;
        return this;
    }

    public Float getPosition() {
        return position;
    }

    public SoundParams setPosition(Float position) {
        this.position = position;
        return this;
    }

    public Float getPitch() {
        return pitch;
    }

    public SoundParams setPitch(Float pitch) {
        this.pitch = pitch;
        return this;
    }

    public Boolean getLoop() {
        return loop;
    }

    public SoundParams setLoop(Boolean loop) {
        this.loop = loop;
        return this;
    }
}
