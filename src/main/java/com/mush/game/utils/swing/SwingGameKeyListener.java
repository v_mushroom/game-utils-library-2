package com.mush.game.utils.swing;

import com.mush.game.utils.input.GameKeyListener;
import com.mush.game.utils.input.GameKeyMapping;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashSet;
import java.util.Set;

public class SwingGameKeyListener implements KeyListener, GameKeyListener {

    private final Set<GameKeyMapping> gameKeyboards = new HashSet<>();

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();
        for (GameKeyMapping gameKeyboard : gameKeyboards) {
            gameKeyboard.keyCodePressed(keyCode);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int keyCode = e.getKeyCode();
        for (GameKeyMapping gameKeyboard : gameKeyboards) {
            gameKeyboard.keyCodeReleased(keyCode);
        }
    }

    @Override
    public void addKeyMapping(GameKeyMapping gameKeyboard) {
        gameKeyboards.add(gameKeyboard);
    }

    @Override
    public void removeKeyMapping(GameKeyMapping gameKeyboard) {
        gameKeyboards.remove(gameKeyboard);
    }
}
