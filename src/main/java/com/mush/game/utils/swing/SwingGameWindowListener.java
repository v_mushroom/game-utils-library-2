package com.mush.game.utils.swing;

import com.mush.game.utils.core.Game;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class SwingGameWindowListener implements WindowListener {

    private final SwingGameWindow window;

    public SwingGameWindowListener(SwingGameWindow window) {
        this.window = window;
    }

    private Game game() {
        return window.getGame();
    }

    public void windowOpened(WindowEvent e) {
    }

    public void windowClosing(WindowEvent e) {
        game().getRefreshLoop().stop();
    }

    public void windowClosed(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
        // if needed disable refreshing
    }

    public void windowDeiconified(WindowEvent e) {
        // if needed enable refreshing
    }

    public void windowActivated(WindowEvent e) {
        if (game().isPauseOnLoseFocus()) {
            game().unpause();
        }
    }

    public void windowDeactivated(WindowEvent e) {
        if (game().isPauseOnLoseFocus()) {
            game().pause();
        }
    }

}
