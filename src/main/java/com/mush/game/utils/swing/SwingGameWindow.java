package com.mush.game.utils.swing;

import com.mush.game.utils.core.Game;
import com.mush.game.utils.core.GameWindow;
import com.mush.game.utils.core.GameWindowConfig;
import com.mush.game.utils.render.GameRenderer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;
import java.util.logging.Level;
import java.util.logging.Logger;

import static javax.swing.JFrame.EXIT_ON_CLOSE;

public class SwingGameWindow implements GameWindow {

    private final Logger logger = Logger.getLogger(this.getClass().getSimpleName());
    private final SwingGameKeyListener keyListener;
    private final Dimension preferredSize = new Dimension(100, 100);
    private final Game game;
    private final GameWindowConfig windowConfig;
    private SwingGamePanel panel;
    private JFrame frame;
    private boolean useActiveGraphics = false;
    private boolean frameUndecorated = false;
    private String frameTitle = "Title";
    private String frameIcon = "img/icon.png";
    private BufferStrategy bufferStrategy = null;

    public SwingGameWindow(Game game, GameWindowConfig windowConfig) {
        this.game = game;
        this.windowConfig = windowConfig;
        preferredSize.width = windowConfig.getWindowWidth();
        preferredSize.height = windowConfig.getWindowHeight();
        keyListener = new SwingGameKeyListener();
    }

    public SwingGameWindow(Game game, int width, int height) {
        this(game, new GameWindowConfig(width, height, 1));
    }

    @Override
    public GameWindowConfig getConfig() {
        return windowConfig;
    }

    public void create() {
        createFrame();
        frame.setVisible(true);
    }

    @Override
    public GameWindow setTitle(String title) {
        this.frameTitle = title;
        if (frame != null) {
            frame.setTitle(title);
        }
        return this;
    }

    @Override
    public void resize(int width, int height) {
        logger.log(Level.INFO, "Window resize to {0} x {1}", new Object[]{width, height});
        preferredSize.width = width;
        preferredSize.height = height;
        panel.setPreferredSize(preferredSize);
        // TODO: consider insets
        frame.setPreferredSize(preferredSize);
        frame.pack();
        frame.setLocationRelativeTo(null);
    }

    public Dimension getDisplaySize(Dimension dimension0) {
        Dimension dimension = dimension0 != null ? dimension0 : new Dimension();
        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        dimension.width = gd.getDisplayMode().getWidth();
        dimension.height = gd.getDisplayMode().getHeight();
        return dimension;
    }

    @Override
    public void resizeToScreen(boolean scalePixels) {
        Dimension display = getDisplaySize(null);

        double wScale = (double) display.width / windowConfig.getWindowWidth();
        double hScale = (double) display.height / windowConfig.getWindowHeight();
        double scale = Math.min(wScale, hScale);

        resize(display.width, display.height);

        GameRenderer renderer = game.getRenderer();
        if (renderer != null) {
            int displayedWidth = (int) (windowConfig.getWindowWidth() * scale);
            int displayedHeight = (int) (windowConfig.getWindowHeight() * scale);

            int xOfs = (display.width - displayedWidth) / 2;
            int yOfs = (display.height - displayedHeight) / 2;

            renderer.setPixelScale(windowConfig.getPixelSize() * scale);
            renderer.setContentOffset(xOfs, yOfs);
        }
    }

    @Override
    public void resizeToConfig() {
        game.getRenderer().setPixelScale(windowConfig.getPixelSize());
        resize(windowConfig.getWindowWidth(), windowConfig.getWindowHeight());
        game.getRenderer().setContentOffset(0, 0);
    }

    @Override
    public void repaint() {
        if (panel != null) {
            if (useActiveGraphics && bufferStrategy != null) {
                panel.activeRepaint(bufferStrategy, frame);
            } else {
                panel.repaint();
            }
        }
    }

    @Override
    public void close() {
        frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
    }

    public void setUseActiveGraphics(boolean useActiveGraphics) {
        this.useActiveGraphics = useActiveGraphics;
    }

    public void setFrameUndecorated(boolean frameUndecorated) {
        this.frameUndecorated = frameUndecorated;
    }

    public void setFrameTitle(String frameTitle) {
        this.frameTitle = frameTitle;
    }

    public void setFrameIcon(String frameIcon) {
        this.frameIcon = frameIcon;
    }

    public Game getGame() {
        return game;
    }

    @Override
    public SwingGameKeyListener getGameKeyListener() {
        return keyListener;
    }

    private void createFrame() {
        frame = new JFrame(frameTitle);

        frame.addWindowListener(new SwingGameWindowListener(this));

        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setUndecorated(frameUndecorated);
        frame.setResizable(false);

        ImageIcon img = new ImageIcon(frameIcon);
        frame.setIconImage(img.getImage());

        if (keyListener != null) {
            frame.addKeyListener(keyListener);
        }

        panel = new SwingGamePanel(game);
        panel.setVisible(true);

        panel.setPreferredSize(preferredSize);

//        frame.setPreferredSize(preferredSize);
        frame.add(panel);

        frame.pack();

        frame.setLocationRelativeTo(null);

        if (useActiveGraphics) {
            frame.setIgnoreRepaint(true);
            frame.createBufferStrategy(3);
            bufferStrategy = frame.getBufferStrategy();
        }
    }
}
