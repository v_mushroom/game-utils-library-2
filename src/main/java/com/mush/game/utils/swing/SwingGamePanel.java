package com.mush.game.utils.swing;

import com.mush.game.utils.core.Game;
import com.mush.game.utils.render.GameRenderer;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferStrategy;

public class SwingGamePanel extends JPanel {

    private final Game game;

    SwingGamePanel(Game game) {
        super();
        this.game = game;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (game != null) {
            GameRenderer renderer = game.getRenderer();
            if (renderer != null) {
                renderer.draw((Graphics2D) g);
            }
        }
    }

    private void activeRepaint(BufferStrategy bufferStrategy, int x, int y) {
        Graphics g;
        try {
            g = bufferStrategy.getDrawGraphics();
        } catch (Exception e) {
            return;
        }
        if (game != null) {
            GameRenderer renderer = game.getRenderer();
            if (renderer != null) {
                Graphics2D g2 = (Graphics2D) g;
                AffineTransform t = g2.getTransform();
                g.translate(x, y);
                renderer.draw(g2);
                g2.setTransform(t);
            }
        }
        bufferStrategy.show();
        g.dispose();
    }

    public void activeRepaint(BufferStrategy bufferStrategy, JFrame frame) {
        Insets insets = frame.getInsets();
        if (insets != null) {
            activeRepaint(bufferStrategy, insets.left, insets.top);
        }
    }
}
