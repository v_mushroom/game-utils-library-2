package com.mush.game.utils.mob;

import java.awt.geom.Rectangle2D;

public class BisectingMobCollisionResolver extends MobCollisionProcessor {

    @Override
    public void process(MovableObject mob1, MovableObject mob2, Rectangle2D.Double frame1, Rectangle2D.Double frame2, double elapsedSeconds) {
        Rectangle2D.Double tempFrame1 = new Rectangle2D.Double();
        Rectangle2D.Double tempFrame2 = new Rectangle2D.Double();
        tempFrame1.setRect(frame1);
        tempFrame2.setRect(frame2);
        double x1 = mob1.getX() - mob1.getVelocityX() * elapsedSeconds;
        double y1 = mob1.getY() - mob1.getVelocityY() * elapsedSeconds;
        double x2 = mob2.getX() - mob2.getVelocityX() * elapsedSeconds;
        double y2 = mob2.getY() - mob2.getVelocityY() * elapsedSeconds;
        double v1 = mob1.getSpeed() * elapsedSeconds;
        double v2 = mob2.getSpeed() * elapsedSeconds;

        BisectionSolver.solve(t -> {
            double time = t * elapsedSeconds;
            tempFrame1.x = x1 + time * mob1.getVelocityX();
            tempFrame1.y = y1 + time * mob1.getVelocityY();
            tempFrame2.x = x2 + time * mob2.getVelocityX();
            tempFrame2.y = y2 + time * mob2.getVelocityY();
            return tempFrame1.intersects(tempFrame2);
        }, step -> {
            return (v1 * step) > 1 || (v2 * step) > 1;
        });
    }
}
