package com.mush.game.utils.mob;

import com.mush.game.utils.event.GameEventQueue;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.List;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class MovableObjectContainer {
    private final AtomicInteger nextObjectId = new AtomicInteger(0);
    private final Map<Integer, MovableObject> objectIndex = new HashMap<>();
    private final List<MovableObject> renderList = new ArrayList<>();
    private final Set<CommutativeValuePair> collisions = new HashSet<>();
    private final Set<CommutativeValuePair> collisionGroups = new HashSet<>();
    private final Point2D.Double position = new Point2D.Double();
    private MobCollisionProcessor collisionProcessor = null;
    private boolean isRenderListDirty = false;
    private int clipWidth;
    private int clipHeight;
    private boolean clipFrame = false;

    public MovableObject createObject() {
        int mobId = nextObjectId.addAndGet(1);
        MovableObject mob = new MovableObject(mobId, this);
        objectIndex.put(mobId, mob);
        renderList.add(mob);
        isRenderListDirty = true;
        return mob;
    }

    public void removeObject(int mobId) {
        MovableObject mob = objectIndex.get(mobId);
        if (mob != null) {
            mob.remove();
            renderList.remove(mob);
            objectIndex.remove(mobId, mob);
        }
    }

    public Set<CommutativeValuePair> getCollisionGroups() {
        return collisionGroups;
    }

    public Optional<MovableObject> getObject(int mobId) {
        return Optional.ofNullable(objectIndex.get(mobId));
    }

    void setRenderListDirty() {
        isRenderListDirty = true;
    }

    public void setCollisionProcessor(MobCollisionProcessor collisionProcessor) {
        this.collisionProcessor = collisionProcessor;
    }

    public void update(double elapsedSeconds) {
        renderList.forEach(object -> object.update(elapsedSeconds));
        Set<CommutativeValuePair> newCollisions = detectCollisions(elapsedSeconds);
        newCollisions.forEach(collision -> {
            if (!collisions.contains(collision)) {
                GameEventQueue.global().sendEvent(new MobCollisionEvent.Started(collision.getValueA(), collision.getValueB()));
            }
        });
        collisions.forEach(collision -> {
            if (!newCollisions.contains(collision)) {
                GameEventQueue.global().sendEvent(new MobCollisionEvent.Finished(collision.getValueA(), collision.getValueB()));
            }
        });
        collisions.clear();
        collisions.addAll(newCollisions);
    }

    public void setPosition(double x, double y) {
        position.setLocation(x, y);
    }

    public void setClipFrame(int w, int h) {
        clipFrame = true;
        clipWidth = w;
        clipHeight = h;
    }

    public void clearClipFrame() {
        clipFrame = false;
    }

    public void draw(Graphics2D g) {
        if (isRenderListDirty) {
            renderList.sort(this::compareMobLayers);
            isRenderListDirty = false;
        }

        Shape clip = null;
        if (clipFrame) {
            clip = g.getClip();
            g.clipRect(0, 0, clipWidth, clipHeight);
        }

        AffineTransform t = g.getTransform();
        g.translate((int) position.x, (int) position.y);
        renderList.forEach(mob -> mob.draw(g));
        g.setTransform(t);

        if (clipFrame) {
            g.setClip(clip);
        }
    }

    private int compareMobLayers(MovableObject mob1, MovableObject mob2) {
        if (mob1.getLayer() != mob2.getLayer()) {
            return Integer.compare(mob1.getLayer(), mob2.getLayer());
        } else {
            return Integer.compare(mob1.getMobId(), mob2.getMobId());
        }
    }

    private Set<CommutativeValuePair> detectCollisions(double elapsedSeconds) {
        List<MovableObject> checkList = renderList.stream()
                .filter(mob -> mob.getArea() != null)
                .filter(mob -> mob.getCollisionGroup() != null)
                .collect(Collectors.toList());

        List<Rectangle2D.Double> frames = checkList.stream()
                .map(mob -> new Rectangle2D.Double(mob.getX() + mob.getArea().getX(), mob.getY() + mob.getArea().getY(), mob.getArea().getWidth(), mob.getArea().getHeight()))
                .collect(Collectors.toList());

        Set<CommutativeValuePair> newCollisions = new HashSet<>();
        Map<Integer, Rectangle2D.Double> mobFrames = new HashMap<>();
        CommutativeValuePair group = new CommutativeValuePair(0, 0);

        for (int i = 0; i < checkList.size(); i++) {
            MovableObject mob1 = checkList.get(i);
            Rectangle2D.Double frame1 = frames.get(i);
            for (int j = i + 1; j < checkList.size(); j++) {
                MovableObject mob2 = checkList.get(j);
                group.modify(mob1.getCollisionGroup(), mob2.getCollisionGroup());

                if (collisionGroups.contains(group)) {
                    Rectangle2D.Double frame2 = frames.get(j);
                    if (frame1.intersects(frame2)) {
                        newCollisions.add(new CommutativeValuePair(mob1.getMobId(), mob2.getMobId()));
                        if (collisionProcessor != null) {
                            mobFrames.put(mob1.getMobId(), frame1);
                            mobFrames.put(mob2.getMobId(), frame2);
                        }
                    }
                }
            }
        }

        if (collisionProcessor != null) {
            newCollisions.forEach(collision -> {
                Optional<MovableObject> mob1 = getObject(collision.getValueA());
                Optional<MovableObject> mob2 = getObject(collision.getValueB());
                Rectangle2D.Double frame1 = mobFrames.get(collision.getValueA());
                Rectangle2D.Double frame2 = mobFrames.get(collision.getValueB());
                if (mob1.isPresent() && mob2.isPresent() && frame1 != null && frame2 != null) {
                    collisionProcessor.process(mob1.get(), mob2.get(), frame1, frame2, elapsedSeconds);
                }
            });
        }

        return newCollisions;
    }
}
