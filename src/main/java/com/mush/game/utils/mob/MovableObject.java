package com.mush.game.utils.mob;

import com.mush.game.utils.event.GameEventQueue;
import com.mush.game.utils.sprite.Sprite;
import com.mush.game.utils.sprite.SpriteFrame;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import static com.mush.game.utils.core.GameDebug.gameDebug;

public class MovableObject {

    private final Integer mobId;
    private final Point2D.Double position;
    private final Point2D.Double velocity;
    private final Rectangle2D.Double area;
    private Sprite sprite;
    private int layer;
    private Integer collisionGroup;
    private MovableObjectContainer container;

//    mob<->mobc, mob.setLayer->mobc.dirty, mob.setPosition->optionalFunction->mob.setLayer

    public MovableObject(int mobId, MovableObjectContainer container) {
        this.mobId = mobId;
        this.container = container;
        position = new Point2D.Double();
        velocity = new Point2D.Double();
        area = new Rectangle2D.Double(-2, -2, 5, 5);
        layer = 0;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }

    public Integer getMobId() {
        return mobId;
    }

    public void remove() {
        container = null;
    }

    public int getLayer() {
        return layer;
    }

    public void setLayer(int newLayer) {
        int oldLayer = this.layer;
        this.layer = newLayer;
        if (container != null && oldLayer != newLayer) {
            container.setRenderListDirty();
        }
    }

    public double getX() {
        return position.getX();
    }

    public double getY() {
        return position.getY();
    }

    public void setPosition(double x, double y) {
        position.setLocation(x, y);
    }

    public double getVelocityX() {
        return velocity.getX();
    }

    public double getVelocityY() {
        return velocity.getY();
    }

    public double getSpeed() {
        return velocity.distance(0, 0);
    }

    public void setVelocity(double x, double y) {
        velocity.setLocation(x, y);
    }

    public Integer getCollisionGroup() {
        return collisionGroup;
    }

    public void setCollisionGroup(Integer collisionGroup) {
        this.collisionGroup = collisionGroup;
    }

    public void update(double elapsedSeconds) {
        position.x += velocity.x * elapsedSeconds;
        position.y += velocity.y * elapsedSeconds;
        if (sprite != null) {
            boolean wasFinished = sprite.isAnimationFinished();
            sprite.update(elapsedSeconds);
            if (sprite.isAnimationFinished() && !wasFinished) {
                GameEventQueue.global().sendEvent(new MobAnimationFinishedEvent(mobId, sprite.getState()));
            }
        }
    }

    public void frameSprite() {
        if (sprite != null) {
            SpriteFrame frame = sprite.getCurrentFrame();
            if (frame != null) {
                area.setRect(-frame.getFrameCenterX(), -frame.getFrameCenterY(), frame.getWidth(), frame.getHeight());
            }
        }
    }

    public Rectangle2D.Double getArea() {
        return area;
    }

    public void setArea(double x, double y, double w, double h) {
        area.setRect(x, y, w, h);
    }

    public void draw(Graphics2D g) {
        AffineTransform t = g.getTransform();
        g.translate((int) position.x, (int) position.y);
        if (sprite != null) {
            sprite.draw(g, 0, 0);
        }
        g.setColor(Color.YELLOW);
        if (gameDebug().drawMobCenter) {
            g.drawLine(-1, 0, 1, 0);
            g.drawLine(0, -1, 0, 1);
        }
        if (gameDebug().drawMobFrame) {
            g.drawRect((int) area.getX(), (int) area.getY(), (int) area.getWidth() - 1, (int) area.getHeight() - 1);
        }
        g.setTransform(t);
    }
}
