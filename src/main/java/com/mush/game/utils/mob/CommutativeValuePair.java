package com.mush.game.utils.mob;

import java.util.Objects;

/**
 * Commutative pair of values.
 * (a,b) == (b,a)
 */
public class CommutativeValuePair {
    private int valueA;
    private int valueB;

    public CommutativeValuePair(int valueA, int valueB) {
        modify(valueA, valueB);
    }

    public int getValueA() {
        return valueA;
    }

    public int getValueB() {
        return valueB;
    }

    public void modify(int valueA, int valueB) {
        this.valueA = valueA;
        this.valueB = valueB;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommutativeValuePair that = (CommutativeValuePair) o;
        return valueA == that.valueA ? valueB == that.valueB
                : valueA == that.valueB && valueB == that.valueA;
    }

    @Override
    public int hashCode() {
        return valueA > valueB
                ? Objects.hash(valueA, valueB)
                : Objects.hash(valueB, valueA);
    }
}
