package com.mush.game.utils.mob;

import java.util.function.Function;

public class BisectionSolver {

    private final static double FACTOR = 0.5;

    public static double solve(Function<Double, Boolean> positionEvaluator, Function<Double, Boolean> stepEvaluator) {
        double position = 0.0;
        double step = FACTOR;

        if (positionEvaluator.apply(position)) {
//            System.out.println("result zero");
            return position;
        }

        while (stepEvaluator.apply(step)) {
//            System.out.println("p:" + position + " s:" + step);
            double p0 = position;
            position += step;

            if (positionEvaluator.apply(position)) {
                // go back
//                System.out.println("hit");
                position = p0;
            }

            step *= FACTOR;
        }

//        System.out.println("result:" + position);
        return position;
    }
}
