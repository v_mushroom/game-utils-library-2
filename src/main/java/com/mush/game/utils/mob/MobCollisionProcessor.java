package com.mush.game.utils.mob;

import java.awt.geom.Rectangle2D;

public abstract class MobCollisionProcessor {
    public abstract void process(MovableObject mob1, MovableObject mob2, Rectangle2D.Double frame1, Rectangle2D.Double frame2, double elapsedSeconds);
}
