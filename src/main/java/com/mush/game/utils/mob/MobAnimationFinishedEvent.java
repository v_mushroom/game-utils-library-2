package com.mush.game.utils.mob;

public class MobAnimationFinishedEvent {
    public final int mobId;
    public final String state;

    public MobAnimationFinishedEvent(int mobId, String state) {
        this.mobId = mobId;
        this.state = state;
    }
}
