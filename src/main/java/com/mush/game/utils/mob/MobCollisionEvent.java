package com.mush.game.utils.mob;

public class MobCollisionEvent {
    public final int mobAId;
    public final int mobBId;

    private MobCollisionEvent(int mobAId, int mobBId) {
        this.mobAId = mobAId;
        this.mobBId = mobBId;
    }

    public static class Started extends MobCollisionEvent {

        public Started(int mobAId, int mobBId) {
            super(mobAId, mobBId);
        }
    }

    public static class Finished extends MobCollisionEvent {
        public Finished(int mobAId, int mobBId) {
            super(mobAId, mobBId);
        }
    }
}
