package com.mush.game.utils.example;

import com.mush.game.utils.core.Game;
import com.mush.game.utils.core.GameWindowConfig;
import com.mush.game.utils.core.Updatable;
import com.mush.game.utils.event.GameEventQueue;
import com.mush.game.utils.event.OnGameEvent;
import com.mush.game.utils.input.GameInputEvent;
import com.mush.game.utils.input.GameKeyMapping;
import com.mush.game.utils.input.InputAxis;
import com.mush.game.utils.mob.*;
import com.mush.game.utils.render.GameRenderer;
import com.mush.game.utils.render.Renderable;
import com.mush.game.utils.sound.GameSoundEvent;
import com.mush.game.utils.sound.SoundParams;
import com.mush.game.utils.sprite.*;
import com.mush.game.utils.swing.SwingGameWindow;
import com.mush.game.utils.tiles.TiledSurface;
import com.mush.game.utils.tiles.UniformTiledSurfaceDataSource;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static com.mush.game.utils.core.GameDebug.gameDebug;

public class Example implements Updatable, Renderable {

    private final Logger logger = Logger.getLogger(this.getClass().getSimpleName());
    Game game;
    BufferedImage cell;
    Sprite sprite;
    Sprite sprite2;
    Integer mobId;
    MovableObjectContainer mobContainer;
    TiledSurface tiledSurface;
    SpriteFactory spriteFactory;
    double fi = 0;
    InputAxis horizontalInput = new InputAxis();
    InputAxis verticalInput = new InputAxis();
    Integer chordId;
    Integer chordMonoId;
    double timeCount = 0;
    List<Integer> mobIds = new ArrayList<>();

    public Example(Game game) {
        this.game = game;

        GameKeyMapping keyMapping = new GameKeyMapping(GameEventQueue.Categories.INPUT);
        keyMapping.bindActionKey(KeyEvent.VK_F4, KeyEvent.VK_F4);
        keyMapping.bindActionKey(KeyEvent.VK_HOME, KeyEvent.VK_HOME);
        keyMapping.bindActionKey(KeyEvent.VK_END, KeyEvent.VK_END);
        keyMapping.bindActionKey(KeyEvent.VK_O, KeyEvent.VK_O);
        keyMapping.bindActionKey(KeyEvent.VK_P, KeyEvent.VK_P);
        keyMapping.bindActionKey(KeyEvent.VK_V, KeyEvent.VK_V);
        keyMapping.bindStateKey(KeyEvent.VK_A, KeyEvent.VK_A);
        keyMapping.bindStateKey(KeyEvent.VK_D, KeyEvent.VK_D);
        keyMapping.bindStateKey(KeyEvent.VK_W, KeyEvent.VK_W);
        keyMapping.bindStateKey(KeyEvent.VK_S, KeyEvent.VK_S);

        game.getKeyListener().addKeyMapping(keyMapping);

        GameEventQueue.category(GameEventQueue.Categories.INPUT).addEventListener(this);
        GameEventQueue.category(GameEventQueue.Categories.SOUND).addEventListener(this);
        GameEventQueue.global().addEventListener(this);

        game.getAudio().loadSound("chord", "example-resources/chord.ogg");
        game.getAudio().loadSound("chord-mono", "example-resources/chord-mono.ogg");
        game.getAudio().loadSound("yawn", "example-resources/aaah.ogg");

        try {
            SpriteSheet spriteSheet = new SpriteSheet("example-resources/frames.png");
            spriteSheet.setCellSize(32, 32);

            SpriteFrame frame1 = new SpriteFrame(spriteSheet.getCell(2, 0), 0, 0);
            SpriteFrame frame2 = new SpriteFrame(spriteSheet.getCell(2, 1), 0, 0);
            cell = frame1.getFrameImage();

            FrameAnimation animation = new FrameAnimation();
            animation.addFrame(frame1, 0.15);
            animation.addFrame(frame2, 0.25);

            SpriteDefinition definition = new SpriteDefinition("");
            definition.setAnimationForState("", animation);

            sprite = new Sprite(definition);
            sprite.setState("");

            spriteFactory = new SpriteFactory(ExampleSpritesConfig.get());
            sprite2 = new Sprite(spriteFactory.getSpriteDefinition("HEAD"));
            sprite2.setState("default");

            mobContainer = new MovableObjectContainer();
            mobContainer.getCollisionGroups().add(new CommutativeValuePair(1, 2));

            MovableObject movableObject = mobContainer.createObject();
            mobId = movableObject.getMobId();

            movableObject.setSprite(sprite2);
            movableObject.frameSprite();
            movableObject.setPosition(20, 100);
            movableObject.setVelocity(0, 50);
            movableObject.setLayer(0);
            movableObject.setCollisionGroup(1);

//            Sprite sprite3 = new Sprite(spriteFactory.getSpriteDefinition("HEAD"));
//            sprite3.setState("default");
//            MovableObject mob2 = mobContainer.createObject();
//            mob2.sprite = sprite3;
//            mob2.frameSprite();
//            mob2.setPosition(50, 100);

//            Sprite sprite4 = new Sprite(spriteFactory.getSpriteDefinition("SMALL_HEAD"));
//            sprite4.setState("default");
//            MovableObject mob3 = mobContainer.createObject();
//            mob3.sprite = sprite4;
//            mob3.frameSprite();
//            mob3.setPosition(50, 120);

            SpriteFrame frame3 = new SpriteFrame(spriteSheet.getCell(3, 0), 0, 0);
            SpriteFrame frame4 = new SpriteFrame(spriteSheet.getCell(3, 1), 0, 0);
            FrameAnimation tileAnimation = new FrameAnimation();
            tileAnimation.addFrame(frame3, 0.5);
            tileAnimation.addFrame(frame4, 0.5);

            tiledSurface = new TiledSurface(200, 200, frame3.getWidth(), frame3.getHeight());
            tiledSurface.setDataSource(new UniformTiledSurfaceDataSource(tileAnimation));
            tiledSurface.setSmoothTranslate(false);

            mobContainer.setClipFrame(200, 200);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Game.setupLogging();
        Game game = new Game();

        SwingGameWindow window = new SwingGameWindow(game, new GameWindowConfig(400, 300, 2));
        window.setTitle("Example");
        window.setUseActiveGraphics(true);
        window.setFrameUndecorated(false);
        window.create();

        game.getRefreshLoop().setLimitFps(true);
        game.setWindow(window);

        Example example = new Example(game);

        game.setUpdatable(example);

        GameRenderer renderer = new GameRenderer(window.getConfig().getViewWidth(), window.getConfig().getViewHeight());
        game.setRenderer(renderer);

        renderer.setRenderable(example);

//        renderer.setScreenSize(400, 300);
//        renderer.setScreenSize(window.getConfig().getViewWidth(), window.getConfig().getViewHeight());
//        renderer.setPixelScale(2);
        renderer.setPixelScale(window.getConfig().getPixelSize());

        game.setPauseOnLoseFocus(true);

        gameDebug().showFps = true;
        gameDebug().showFpsHistory = true;
//        gameDebug().drawSurfaceFrame = true;
//        gameDebug().drawSurfaceInnerFrame = true;
//        gameDebug().drawSurfaceTileFrames = true;

        game.start();
    }

    @OnGameEvent
    public void onState(GameInputEvent.State state) {
        if (state.message instanceof Integer) {
            switch ((Integer) state.message) {
                case KeyEvent.VK_W:
                    verticalInput.setInput(state.active);
                    break;
                case KeyEvent.VK_S:
                    verticalInput.setReverseInput(state.active);
                    break;
                case KeyEvent.VK_A:
                    horizontalInput.setReverseInput(state.active);
                    break;
                case KeyEvent.VK_D:
                    horizontalInput.setInput(state.active);
                    break;
            }
        }
    }

    @OnGameEvent
    public void onAction(GameInputEvent.Action action) {
        if (action.message instanceof Integer) {
            Integer soundId;
            switch ((Integer) action.message) {
                case KeyEvent.VK_F4:
                    game.getWindow().close();
                    break;
                case KeyEvent.VK_HOME:
                    game.getWindow().resizeToScreen(true);
                    break;
                case KeyEvent.VK_END:
                    game.getWindow().resizeToConfig();
                    break;
                case KeyEvent.VK_O:
                    soundId = game.getAudio().playSound("chord-mono");
                    if (chordMonoId == null) {
                        chordMonoId = soundId;
                    }
                    break;
                case KeyEvent.VK_P:
                    soundId = game.getAudio().playSound("chord");
                    if (chordId == null) {
                        chordId = soundId;
                    }
                    break;
                case KeyEvent.VK_V:
                    gameDebug().drawMobCenter = !gameDebug().drawMobCenter;
                    gameDebug().drawMobFrame = !gameDebug().drawMobFrame;
                    break;
            }
        }
    }

    @OnGameEvent
    public void onSoundEvent(GameSoundEvent.SoundStarted event) {
        logger.info("Sound started: " + event.soundId + " - " + game.getAudio().getSoundNameForSoundId(event.soundId));
    }

    @OnGameEvent
    public void onSoundEvent(GameSoundEvent.SoundFinished event) {
        logger.info("Sound finished: " + event.soundId + " - " + game.getAudio().getSoundNameForSoundId(event.soundId));
        if (event.soundId.equals(chordId)) {
            chordId = null;
        } else if (event.soundId.equals(chordMonoId)) {
            chordMonoId = null;
        }
    }

    @OnGameEvent
    public void onAnimationFinished(MobAnimationFinishedEvent event) {
        logger.info("Mob animation finished: " + event.mobId + " - " + event.state);
        mobContainer.getObject(event.mobId).ifPresent(mob -> mob.getSprite().setState("default"));
    }

    @OnGameEvent
    public void onMobCollision(MobCollisionEvent.Started event) {
        logger.info("Mob collision started: " + event.mobAId + " + " + event.mobBId);
        if (event.mobAId != mobId && event.mobBId != mobId) {
            return;
        }
        int id = event.mobAId != mobId ? event.mobAId : event.mobBId;
        mobContainer.getObject(id).ifPresent(mob -> {
            if ("SMALL_HEAD".equals(mob.getSprite().getType())) {
                mob.getSprite().setState("wobble");
            }
        });
    }

    @OnGameEvent
    public void onMobCollision(MobCollisionEvent.Finished event) {
        logger.info("Mob collision finished: " + event.mobAId + " + " + event.mobBId);
        if (event.mobAId != mobId && event.mobBId != mobId) {
            return;
        }
        int id = event.mobAId != mobId ? event.mobAId : event.mobBId;
        mobContainer.getObject(id).ifPresent(mob -> {
            if ("SMALL_HEAD".equals(mob.getSprite().getType())) {
                mob.getSprite().setState("default");
            }
        });
    }

    @Override
    public void update(double elapsedSeconds) {
        sprite.update(elapsedSeconds);
        mobContainer.update(elapsedSeconds);

        tiledSurface.move(Math.sin(fi) * 50.0 * elapsedSeconds, Math.sin(fi / 3) * 50.0 * elapsedSeconds);
        fi += elapsedSeconds * Math.PI;
        tiledSurface.update(elapsedSeconds);

        mobContainer.setPosition(tiledSurface.getPosition().getX(), tiledSurface.getPosition().getY());
        mobContainer.getObject(mobId).ifPresent(mob -> {
            if (mob.getY() > 150) {
                mob.setPosition(mob.getX(), 50);
            }
        });

        timeCount += elapsedSeconds;
        if (timeCount > 1.0) {
            timeCount = 0;
            MovableObject newMob = mobContainer.createObject();
            newMob.setSprite(new Sprite(spriteFactory.getSpriteDefinition("SMALL_HEAD")));
            newMob.getSprite().setState("default");
            newMob.setCollisionGroup(2);
            newMob.frameSprite();
            if (Math.random() < 0.5) {
                newMob.setPosition(24 + Math.random() * 15, 100 + Math.random() * 30);
                newMob.setLayer(10);
            } else {
                newMob.setPosition(16 - Math.random() * 15, 100 + Math.random() * 30);
                newMob.setLayer(-10);
            }
            mobIds.add(newMob.getMobId());
            if (mobIds.size() > 10) {
                mobContainer.removeObject(mobIds.get(0));
                mobIds.remove(0);
            }
        }

        if (!mobIds.isEmpty() && Math.random() < 0.001) {
            int id = mobIds.get((int) (Math.random() * mobIds.size()));
            mobContainer.getObject(id).ifPresent(mob -> {
                if ("default".equals(mob.getSprite().getState())) {
                    mob.getSprite().setState("yawn");
                    game.getAudio().playSound("yawn");
                }
            });
        }

        if (chordMonoId != null) {
            game.getAudio().modifySound(chordMonoId, new SoundParams().setPosition((float) Math.sin(fi)));
        }
        if (chordId != null) {
            game.getAudio().modifySound(chordId, new SoundParams().setPitch((float) (Math.pow(2, Math.sin(fi * 3)))));
        }
    }

    @Override
    public void updateCurrentFps(double fps) {

    }

    @Override
    public void renderContent(Graphics2D g, GameRenderer renderer) {
        g.setColor(Color.DARK_GRAY);
        g.fillRect(0, 0, renderer.getScreenWidth(), renderer.getScreenHeight());

        AffineTransform t = g.getTransform();
        g.translate(60, 20);
        tiledSurface.draw(g);
        mobContainer.draw(g);
        g.setTransform(t);

        g.setColor(Color.CYAN);
        g.drawRect(10, 10, renderer.getScreenWidth() - 20, renderer.getScreenHeight() - 20);
        g.setColor(Color.YELLOW);
        g.drawLine(10, 10, 10 + (int) (Math.random() * 100), 10 + (int) (Math.random() * 100));

        g.drawImage(cell, null, 100, 100);

        sprite.draw(g, 20, 50);

//        mobContainer.draw(g);

        g.setColor(Color.GREEN);
        int x0 = renderer.getScreenWidth() / 2;
        int y0 = renderer.getScreenHeight() / 2;
        g.drawLine(x0, y0, x0 + (int) (horizontalInput.getValue() * 10), y0 - (int) (verticalInput.getValue() * 10));
    }
}
