package com.mush.game.utils.example;

import com.mush.game.utils.sprite.SpriteFactoryConfigBuilder;
import com.mush.game.utils.sprite.SpriteFactoryConfiguration;

import static com.mush.game.utils.sprite.SpriteFactoryConfigBuilder.spriteState;
import static com.mush.game.utils.sprite.SpriteFactoryConfigBuilder.spriteType;

public class ExampleSpritesConfig {

    public static SpriteFactoryConfiguration get() {
        return new SpriteFactoryConfigBuilder()
                .spriteSheet("example-resources/frames.png")
                .cellSize(32, 32)
                .center(0, 0)
                .addType("BOX", spriteType()
                        .addState("default", spriteState()
                                .center(16, 16)
                                .addFrame(0, 0, 1.0)
                                .addFrame(0, 1, 0.2)
                                .addFrame(1, 0, 1.0)
                                .addFrame(1, 1, 1.0)))
                .addType("HEAD", spriteType()
                        .uvOffset(8, 0)
                        .cellSize(16, 16)
                        .addState("default", spriteState()
                                .center(8, 8)
                                .addFrame(0, 0, 2.0)
                                .addFrame(1, 0, 0.2)))
                .addType("SMALL_HEAD", spriteType()
                        .uvOffset(16, 2)
                        .cellSize(8, 8)
                        .center(4, 4)
                        .addState("default", spriteState()
                                .addFrame(0, 0))
                        .addState("yawn", spriteState()
                                .addFrame(1, 0, 0.1)
                                .addFrame(2, 0, 0.1)
                                .addFrame(3, 0, 0.3)
                                .addFrame(2, 0, 0.2)
                                .addFrame(1, 0, 0.1)
                                .doesNotRepeat())
                        .addState("wobble", spriteState()
                                .addFrame(0, 1, 0.1)
                                .addFrame(1, 1, 0.1)
                                .addFrame(2, 1, 0.1)
                                .addFrame(3, 1, 0.1)
                                .repeats()))
                .build();
    }
}
