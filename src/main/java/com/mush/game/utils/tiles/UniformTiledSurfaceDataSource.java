package com.mush.game.utils.tiles;

import com.mush.game.utils.sprite.FrameAnimation;

public class UniformTiledSurfaceDataSource implements TiledSurfaceDataSource {

    FrameAnimation animation;

    public UniformTiledSurfaceDataSource(FrameAnimation animation) {
        this.animation = animation;
    }

    @Override
    public FrameAnimation getSurfaceData(int u, int v) {
        return animation;
    }
}
