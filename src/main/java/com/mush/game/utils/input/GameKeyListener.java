package com.mush.game.utils.input;

public interface GameKeyListener {

    void addKeyMapping(GameKeyMapping gameKeyboard);

    void removeKeyMapping(GameKeyMapping gameKeyboard);
}
