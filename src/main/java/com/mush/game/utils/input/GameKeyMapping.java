/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.input;

import com.mush.game.utils.event.GameEventQueue;
import com.mush.game.utils.event.SimpleGameEventQueue;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mush
 */
public class GameKeyMapping {

    private final Map<Integer, Boolean> keyStateMap;
    private final Map<Integer, Object> actionKeyMap;
    private final Map<Integer, Object> stateKeyMap;
    
    private Object queueCategory;

    /**
     * Using global game event queue
     */
    public GameKeyMapping() {
        this(null);
    }
    
    /**
     * Using category game event queue
     * @param category 
     */
    public GameKeyMapping(Object category) {
        this.keyStateMap = new HashMap<>();
        this.actionKeyMap = new HashMap<>();
        this.stateKeyMap = new HashMap<>();
        this.queueCategory = category;
    }

    public void keyCodePressed(int keyCode) {
        Boolean previous = keyStateMap.put(keyCode, Boolean.TRUE);
        if (!Boolean.TRUE.equals(previous)) {
            onKeyDown(keyCode);
        }
    }

    public void keyCodeReleased(int keyCode) {
        Boolean previous = keyStateMap.put(keyCode, Boolean.FALSE);
        if (!Boolean.FALSE.equals(previous)) {
            onKeyUp(keyCode);
        }
    }

    public void bindActionKey(int keyCode, Object message) {
        actionKeyMap.put(keyCode, message);
    }

    public void bindStateKey(int keyCode, Object message) {
        stateKeyMap.put(keyCode, message);
    }

    private void onKeyDown(int keyCode) {
        if (actionKeyMap.containsKey(keyCode)) {
            getEventQueue().sendEvent(new GameInputEvent.Action(actionKeyMap.get(keyCode)));
        }
        if (stateKeyMap.containsKey(keyCode)) {
            getEventQueue().sendEvent(new GameInputEvent.State(stateKeyMap.get(keyCode), true));
        }
    }

    private void onKeyUp(int keyCode) {
        if (stateKeyMap.containsKey(keyCode)) {
            getEventQueue().sendEvent(new GameInputEvent.State(stateKeyMap.get(keyCode), false));
        }
    }
    
    private SimpleGameEventQueue getEventQueue() {
        return queueCategory == null ? GameEventQueue.global() : GameEventQueue.category(queueCategory);
    }

}
