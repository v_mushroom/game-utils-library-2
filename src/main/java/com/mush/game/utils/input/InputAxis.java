/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.game.utils.input;

/**
 * @author mush
 */
public class InputAxis {

    private double input;
    private double reverseInput;
    private double value;

    public InputAxis() {
        setInput(0);
        setReverseInput(0);
    }

    public void setInput(double value) {
        this.input = value;
        evaluate();
    }

    public void setInput(boolean value) {
        setInput(value ? 1 : 0);
    }

    public void setReverseInput(double value) {
        this.reverseInput = value;
        evaluate();
    }

    public void setReverseInput(boolean value) {
        setReverseInput(value ? 1 : 0);
    }

    private void evaluate() {
        value = input - reverseInput;
    }

    public double getValue() {
        return value;
    }

}
