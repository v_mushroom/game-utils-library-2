package com.mush.game.utils.core;

public class GameWindowConfig {

    private final int viewWidth;
    private final int viewHeight;
    private final int pixelSize;

//    private int contentOffsetX;
//    private int contentOffsetY;

    public GameWindowConfig(int viewWidth, int viewHeight, int pixelSize) {
        this.viewWidth = viewWidth;
        this.viewHeight = viewHeight;
        this.pixelSize = pixelSize;
//        setContentOffset(0, 0);
    }

    public int getViewWidth() {
        return viewWidth;
    }

    public int getViewHeight() {
        return viewHeight;
    }

    public int getPixelSize() {
        return pixelSize;
    }

    public int getPixelWidth() {
        return viewWidth / pixelSize;
    }

    public int getPixelHeight() {
        return viewHeight / pixelSize;
    }

    public int getWindowWidth() {
        return viewWidth * pixelSize;
    }

    public int getWindowHeight() {
        return viewHeight * pixelSize;
    }

//    public void setContentOffset(int x, int y) {
//        contentOffsetX = x;
//        contentOffsetY = y;
//    }
//
//    public int getContentOffsetX() {
//        return contentOffsetX;
//    }
//
//    public int getContentOffsetY() {
//        return contentOffsetY;
//    }
}
