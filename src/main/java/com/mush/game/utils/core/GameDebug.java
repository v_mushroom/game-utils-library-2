package com.mush.game.utils.core;

public class GameDebug {
    private static final GameDebug INSTANCE = new GameDebug();

    public boolean showFps = false;
    public boolean showFpsHistory = false;
    public boolean drawSurfaceFrame = false;
    public boolean drawSurfaceInnerFrame = false;
    public boolean drawSurfaceTileFrames = false;
    public boolean drawMobCenter = true;
    public boolean drawMobFrame = true;

    public static GameDebug gameDebug() {
        return INSTANCE;
    }
}
