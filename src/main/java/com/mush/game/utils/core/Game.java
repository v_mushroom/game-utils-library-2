package com.mush.game.utils.core;

import com.mush.game.utils.event.GameEventQueue;
import com.mush.game.utils.input.GameKeyListener;
import com.mush.game.utils.render.GameRenderer;
import com.mush.game.utils.sound.GameAudio;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Game implements Updatable {

    private final Logger logger = Logger.getLogger(this.getClass().getSimpleName());
    private final GameRefreshLoop refreshLoop;
    private final Thread refreshThread;
    private final GameAudio gameAudio;

    private GameRenderer renderer;
    private Updatable updatable;
    private GameWindow window;

    private boolean pauseOnLoseFocus = false;
    private boolean renderWhenMinimized = false;

    public Game() {
        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdownHook));

        this.refreshLoop = new GameRefreshLoop(this);
        this.refreshThread = new Thread(refreshLoop);
        this.refreshThread.setName("Game Refresh Thread");
        this.gameAudio = new GameAudio();
    }

    public static void setupLogging() {
        setupLogging("logging.properties");
    }

    public static void setupLogging(String filename) {
        try {
            InputStream stream = Game.class.getClassLoader().
                    getResourceAsStream(filename);
            if (stream != null) {
                LogManager.getLogManager().readConfiguration(stream);
            } else {
                System.setProperty("java.util.logging.SimpleFormatter.format",
                        "[%1$tF %1$tT] [%4$-7s] %5$s %n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Game start() {
        startRefreshThread();
        return this;
    }

    @Override
    public void update(double elapsedSeconds) {
        gameAudio.update();
        GameEventQueue.processQueue();

        if (updatable != null) {
            updatable.update(elapsedSeconds);
        }
    }

    @Override
    public void updateCurrentFps(double fps) {
        if (updatable != null) {
            updatable.updateCurrentFps(fps);
        }
    }

    private void shutdownHook() {
        System.out.println("Game shutdown hook");
        gameAudio.cleanup();
        System.out.println("Shutdown hook completed");
    }

    public Updatable getUpdatable() {
        return updatable;
    }

    public Game setUpdatable(Updatable updatable) {
        this.updatable = updatable;
        return this;
    }

    public GameWindow getWindow() {
        return window;
    }

    public Game setWindow(GameWindow window) {
        this.window = window;
        return this;
    }

    public GameRenderer getRenderer() {
        return renderer;
    }

    public Game setRenderer(GameRenderer renderer) {
        this.renderer = renderer;
        return this;
    }

    private void startRefreshThread() {
        if (refreshThread.isAlive()) {
            return;
        }
        refreshThread.start();
    }

    public GameRefreshLoop getRefreshLoop() {
        return refreshLoop;
    }

    public GameAudio getAudio() {
        return gameAudio;
    }

    public GameKeyListener getKeyListener() {
        return window.getGameKeyListener();
    }

    public boolean isPauseOnLoseFocus() {
        return pauseOnLoseFocus;
    }

    public void setPauseOnLoseFocus(boolean pauseOnLoseFocus) {
        this.pauseOnLoseFocus = pauseOnLoseFocus;
    }

    public boolean isRenderWhenMinimized() {
        return renderWhenMinimized;
    }

    public void setRenderWhenMinimized(boolean renderWhenMinimized) {
        this.renderWhenMinimized = renderWhenMinimized;
    }

    public void unpause() {
        refreshLoop.setUpdating(true);
        gameAudio.unpause();
    }

    public void pause() {
        refreshLoop.setUpdating(false);
        gameAudio.pause();
    }
}
