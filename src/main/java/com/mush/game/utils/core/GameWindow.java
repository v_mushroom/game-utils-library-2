package com.mush.game.utils.core;

import com.mush.game.utils.input.GameKeyListener;

public interface GameWindow {

    void resize(int width, int height);

    void resizeToScreen(boolean scalePixels);

    void resizeToConfig();

    void close();

    GameWindow setTitle(String title);

    void repaint();

    GameKeyListener getGameKeyListener();

    GameWindowConfig getConfig();

}
