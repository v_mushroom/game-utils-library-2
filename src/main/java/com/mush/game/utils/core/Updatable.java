package com.mush.game.utils.core;

public interface Updatable {

    public void update(double elapsedSeconds);

    public void updateCurrentFps(double fps);
}
