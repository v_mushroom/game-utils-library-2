package com.mush.game.utils.core;

import com.mush.game.utils.render.GameRenderer;

import java.util.logging.Level;
import java.util.logging.Logger;

public class GameRefreshLoop implements Runnable {

    public static final long NANO = 1000000000;
    private static final long MICRO = 1000000;
    private static final int DEFAULT_FPS_LIMIT = 30;
    private final Game game;
    private boolean isRunning = true;
    private boolean isUpdating = true;
    private boolean isLimitFps = true;
    private boolean isLimitFpsToTarget = true;
    private long targetNanos;
    private double currentFrameFps;
    private double averageFps;

    public GameRefreshLoop(Game game) {
        this.game = game;
        setLimitFps(true, DEFAULT_FPS_LIMIT);
    }

    private void setTargetFpsLimit(int targetFps) {
        targetNanos = targetFps <= 0
                ? 0
                : NANO / targetFps;
        currentFrameFps = 0;
        averageFps = 0;
    }

    public void stop() {
        isRunning = false;
    }

    public void setUpdating(boolean b) {
        isUpdating = b;
    }

    public void setLimitFps(boolean b) {
        isLimitFps = b;
        isLimitFpsToTarget = false;
    }

    public void setLimitFps(boolean b, int targetFps) {
        isLimitFps = b;
        isLimitFpsToTarget = true;
        setTargetFpsLimit(targetFps);
    }

    @Override
    public void run() {
        long tick = System.nanoTime();
        long lastTick;
        while (isRunning) {
            lastTick = tick;
            tick = System.nanoTime();
            long elapsedNanos = tick - lastTick;
            long overheadNanos = elapsedNanos - targetNanos;
            long availableNanos = overheadNanos > 0 ? targetNanos - overheadNanos : targetNanos;
            double elapsedSeconds = (double) elapsedNanos / NANO;

            update(elapsedSeconds);

            if (isLimitFps) {
                long tock = System.nanoTime();
                long spentNanos = tock - tick;
                long remainNanos = availableNanos - spentNanos;

                long sleepMillis = remainNanos / MICRO;

                try {
                    if (isLimitFpsToTarget && targetNanos > 0) {
                        if (sleepMillis > 0) {
                            long sleptNanos = 0;
                            while (sleptNanos < remainNanos) {
                                // Sleep the shortest possible time, until target sleep duration matched
                                Thread.sleep(0);
                                sleptNanos = System.nanoTime() - tock;
                            }
                        }
                    } else if (sleepMillis > 0) {
                        // Just sleep for some short amount of time, to limit using all resources
                        Thread.sleep(2);
                    }
                } catch (InterruptedException ex) {
                    // Do nothing
                }
            }
        }
    }

    private void update(double elapsedSeconds) {
        updateCurrentFps(elapsedSeconds);

        if (isUpdating) {
            try {
                game.updateCurrentFps(currentFrameFps);
                game.update(elapsedSeconds);
            } catch (Exception e) {
                Logger.getGlobal().log(Level.INFO, "Exception during update", e);
            }

            GameRenderer renderer = game.getRenderer();
            if (renderer != null) {
                renderer.updateFps(currentFrameFps);
                renderer.render();
            }
        }

        if (game.getWindow() != null) {
            try {
                game.getWindow().repaint();
            } catch (Exception e) {
                Logger.getGlobal().log(Level.INFO, "Exception during repaint", e);
            }
        }
    }

    private void updateCurrentFps(double elapsedSeconds) {
        currentFrameFps = elapsedSeconds > 0
                ? 1.0 / elapsedSeconds
                : 1000;
        averageFps = averageFps * 0.9 + currentFrameFps * 0.1;
    }
}
