package com.mush.game.utils.sprite;

import java.awt.*;
import java.util.Objects;

public class Sprite {

    private final SpriteDefinition definition;
    private final FrameAnimation.Advancement animationAdvancement;
    private String state;
    private FrameAnimation animation;
    private int animationFrameIndex;
    private double secondsSinceFrame;
    private SpriteFrame currentFrame;

    public Sprite(SpriteDefinition definition) {
        animationAdvancement = new FrameAnimation.Advancement();
        this.definition = definition;
        this.state = null;
        setAnimation(null);
    }

    public void update(double elapsedSeconds) {
        if (animation == null) {
            return;
        }

        if (animation.hasMultipleFrames() || !animation.repeats()) {
            secondsSinceFrame += elapsedSeconds;

            animation.advance(animationFrameIndex, secondsSinceFrame, animationAdvancement);

            if (animationFrameIndex != animationAdvancement.frameIndex) {
                setFrame(animationAdvancement.frameIndex, animationAdvancement.secondsSinceFrame);
            }
        }
    }

    public void draw(Graphics2D g, int x, int y) {
        if (currentFrame != null) {
            g.drawImage(currentFrame.getFrameImage(), null,
                    x - currentFrame.getFrameCenterX(),
                    y - currentFrame.getFrameCenterY());
        }
    }

    public void setAnimation(FrameAnimation animation) {
        this.animation = animation;
        if (this.animation != null) {
            setFrame(0, 0);
            animationAdvancement.reset();
        } else {
            currentFrame = null;
        }
    }

    public String getType() {
        return definition.getSpriteType();
    }

    public String getState() {
        return this.state;
    }

    public void setState(String newState) {
        if (definition == null) {
            return;
        }
        if (!Objects.equals(newState, this.state)) {
            this.state = newState;
            setAnimation(definition.getAnimationForState(this.state));
        }
    }

    public SpriteFrame getCurrentFrame() {
        return currentFrame;
    }

    public boolean isAnimationFinished() {
        return animationAdvancement.isFinished();
    }

    public Double getAnimationDuration() {
        if (this.animation != null) {
            return this.animation.getTotalDuration();
        } else {
            return null;
        }
    }

    private void setFrame(int frameIndex, double elapsedSeconds) {
        animationFrameIndex = frameIndex;
        secondsSinceFrame = elapsedSeconds;
        currentFrame = animation.getFrame(animationFrameIndex);
    }
}
