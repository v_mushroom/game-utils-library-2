package com.mush.game.utils.sprite;

public class SpriteSheetConfig {

    private SpriteSheetConfig parent;
    private String spriteSheet;
    private Integer cellWidth;
    private Integer cellHeight;
    private Integer uOffset;
    private Integer vOffset;
    private Integer xCenter;
    private Integer yCenter;

    public void setParent(SpriteSheetConfig parent) {
        this.parent = parent;
    }

    public String getSpriteSheet() {
        return spriteSheet != null ? spriteSheet : (parent != null ? parent.getSpriteSheet() : null);
    }

    public void setSpriteSheet(String spriteSheet) {
        this.spriteSheet = spriteSheet;
    }

    public Integer getCellWidth() {
        return cellWidth != null ? cellWidth : (parent != null ? parent.getCellWidth() : 8);
    }

    public void setCellWidth(Integer cellWidth) {
        this.cellWidth = cellWidth;
    }

    public Integer getCellHeight() {
        return cellHeight != null ? cellHeight : (parent != null ? parent.getCellHeight() : 8);
    }

    public void setCellHeight(Integer cellHeight) {
        this.cellHeight = cellHeight;
    }

    public Integer getuOffset() {
        return uOffset != null ? uOffset : (parent != null ? parent.getuOffset() : 0);
    }

    public void setuOffset(Integer uOffset) {
        this.uOffset = uOffset;
    }

    public Integer getvOffset() {
        return vOffset != null ? vOffset : (parent != null ? parent.getvOffset() : 0);
    }

    public void setvOffset(Integer vOffset) {
        this.vOffset = vOffset;
    }

    public Integer getxCenter() {
        return xCenter != null ? xCenter : (parent != null ? parent.getxCenter() : 0);
    }

    public void setxCenter(Integer xCenter) {
        this.xCenter = xCenter;
    }

    public Integer getyCenter() {
        return yCenter != null ? yCenter : (parent != null ? parent.getyCenter() : 0);
    }

    public void setyCenter(Integer yCenter) {
        this.yCenter = yCenter;
    }
}
