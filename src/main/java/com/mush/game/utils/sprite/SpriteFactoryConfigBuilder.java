package com.mush.game.utils.sprite;

import java.util.ArrayList;
import java.util.HashMap;

public class SpriteFactoryConfigBuilder {

    private final SpriteFactoryConfiguration configuration;

    public SpriteFactoryConfigBuilder() {
        configuration = new SpriteFactoryConfiguration();
    }

    private static void spriteSheet(String spriteSheet, SpriteSheetConfig config) {
        config.setSpriteSheet(spriteSheet);
    }

    private static void cellSize(int w, int h, SpriteSheetConfig config) {
        config.setCellWidth(w);
        config.setCellHeight(h);
    }

    private static void center(int x, int y, SpriteSheetConfig config) {
        config.setxCenter(x);
        config.setyCenter(y);
    }

    private static void uvOffset(int u, int v, SpriteSheetConfig config) {
        config.setuOffset(u);
        config.setvOffset(v);
    }

    public static SpriteTypeBuilder spriteType() {
        return new SpriteTypeBuilder();
    }

    public static SpriteStateBuilder spriteState() {
        return new SpriteStateBuilder();
    }

    private SpriteFactoryConfiguration example() {
        return this
                .spriteSheet("sdfsdf.png")
                .uvOffset(0, 0)
                .cellSize(32, 32)
                .center(0, 0)
                .addType("HEAD", spriteType()
                        .uvOffset(8, 0)
                        .cellSize(16, 16)
                        .addState("default", spriteState()
                                .center(8, 8)
                                .addFrame(0, 0, 1.0)
                                .addFrame(0, 1, 0.2)
                                .repeats()))
                .build();
    }

    public SpriteFactoryConfiguration build() {
        return configuration;
    }

    public SpriteFactoryConfigBuilder spriteSheet(String spriteSheet) {
        spriteSheet(spriteSheet, configuration);
        return this;
    }

    public SpriteFactoryConfigBuilder cellSize(int w, int h) {
        cellSize(w, h, configuration);
        return this;
    }

    public SpriteFactoryConfigBuilder center(int x, int y) {
        center(x, y, configuration);
        return this;
    }

    public SpriteFactoryConfigBuilder uvOffset(int u, int v) {
        uvOffset(u, v, configuration);
        return this;
    }

    public SpriteFactoryConfigBuilder addType(String type, SpriteTypeBuilder typeBuilder) {
        if (configuration.getSpriteTypes() == null) {
            configuration.setSpriteTypes(new HashMap<>());
        }
        configuration.getSpriteTypes().put(type, typeBuilder.typeConfig);
        return this;
    }

    public static class SpriteTypeBuilder {
        private final SpriteFactoryConfiguration.SpriteTypeConfig typeConfig = new SpriteFactoryConfiguration.SpriteTypeConfig();

        public SpriteTypeBuilder spriteSheet(String spriteSheet) {
            SpriteFactoryConfigBuilder.spriteSheet(spriteSheet, typeConfig);
            return this;
        }

        public SpriteTypeBuilder cellSize(int w, int h) {
            SpriteFactoryConfigBuilder.cellSize(w, h, typeConfig);
            return this;
        }

        public SpriteTypeBuilder center(int x, int y) {
            SpriteFactoryConfigBuilder.center(x, y, typeConfig);
            return this;
        }

        public SpriteTypeBuilder uvOffset(int u, int v) {
            SpriteFactoryConfigBuilder.uvOffset(u, v, typeConfig);
            return this;
        }

        public SpriteTypeBuilder addState(String state, SpriteStateBuilder stateBuilder) {
            if (typeConfig.getSpriteStates() == null) {
                typeConfig.setSpriteStates(new HashMap<>());
            }
            typeConfig.getSpriteStates().put(state, stateBuilder.stateConfig);
            return this;
        }

    }

    public static class SpriteStateBuilder {
        private final SpriteFactoryConfiguration.SpriteStateConfig stateConfig = new SpriteFactoryConfiguration.SpriteStateConfig();

        public SpriteStateBuilder spriteSheet(String spriteSheet) {
            SpriteFactoryConfigBuilder.spriteSheet(spriteSheet, stateConfig);
            return this;
        }

        public SpriteStateBuilder cellSize(int w, int h) {
            SpriteFactoryConfigBuilder.cellSize(w, h, stateConfig);
            return this;
        }

        public SpriteStateBuilder center(int x, int y) {
            SpriteFactoryConfigBuilder.center(x, y, stateConfig);
            return this;
        }

        public SpriteStateBuilder uvOffset(int u, int v) {
            SpriteFactoryConfigBuilder.uvOffset(u, v, stateConfig);
            return this;
        }

        public SpriteStateBuilder repeats() {
            stateConfig.setRepeats(true);
            return this;
        }

        public SpriteStateBuilder doesNotRepeat() {
            stateConfig.setRepeats(false);
            return this;
        }

        public SpriteStateBuilder addFrame(int u, int v) {
            return addFrame(u, v, 0.0);
        }

        public SpriteStateBuilder addFrame(int u, int v, double duration) {
            if (stateConfig.getAnimationFrames() == null) {
                stateConfig.setAnimationFrames(new ArrayList<>());
            }
            SpriteFactoryConfiguration.AnimationFrameConfig frameConfig = new SpriteFactoryConfiguration.AnimationFrameConfig();
            frameConfig.setU(u);
            frameConfig.setV(v);
            frameConfig.setDuration(duration);
            stateConfig.getAnimationFrames().add(frameConfig);
            return this;
        }
    }

}
