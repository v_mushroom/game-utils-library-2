package com.mush.game.utils.sprite;

import java.util.HashMap;
import java.util.Map;

public class SpriteDefinition {

    private final String spriteType;
    private final Map<String, FrameAnimation> stateAnimationMap;

    public SpriteDefinition(String spriteType) {
        this.spriteType = spriteType;
        stateAnimationMap = new HashMap<>();
    }

    public String getSpriteType() {
        return spriteType;
    }

    public void setAnimationForState(String state, FrameAnimation animation) {
        stateAnimationMap.put(state, animation);
    }

    public FrameAnimation getAnimationForState(String state) {
        return stateAnimationMap.get(state);
    }
}
