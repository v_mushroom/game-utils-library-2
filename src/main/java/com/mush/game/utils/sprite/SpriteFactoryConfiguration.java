package com.mush.game.utils.sprite;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SpriteFactoryConfiguration extends SpriteSheetConfig {

    private Map<String, SpriteTypeConfig> spriteTypes;

    public void connect() {
        spriteTypes.values().forEach(spriteType -> {
            spriteType.setParent(this);
            spriteType.connect();
        });
    }

    public Map<String, SpriteTypeConfig> getSpriteTypes() {
        return spriteTypes;
    }

    public void setSpriteTypes(Map<String, SpriteTypeConfig> spriteTypes) {
        this.spriteTypes = spriteTypes;
    }

    public static class SpriteTypeConfig extends SpriteSheetConfig {
        private Map<String, SpriteStateConfig> spriteStates;

        private void connect() {
            spriteStates.values().forEach(spriteState -> {
                spriteState.setParent(this);
                spriteState.connect();
            });
        }

        public Map<String, SpriteStateConfig> getSpriteStates() {
            return spriteStates;
        }

        public void setSpriteStates(Map<String, SpriteStateConfig> spriteStates) {
            this.spriteStates = spriteStates;
        }
    }

    public static class SpriteStateConfig extends SpriteSheetConfig {
        private List<AnimationFrameConfig> animationFrames;
        private double[][] frames;
        private boolean repeats = true;
        private int repeatCount = 0;

        private void connect() {
            if (frames != null && animationFrames == null) {
                animationFrames = Stream.of(frames)
//                        .stream()
                        .map(doubles -> {
                            AnimationFrameConfig frame = new AnimationFrameConfig();
                            frame.setU(doubles.length > 0 ? (int) doubles[0] : 0);
                            frame.setV(doubles.length > 1 ? (int) doubles[1] : 0);
                            frame.setDuration(doubles.length > 2 ? doubles[2] : 0.0);
                            return frame;
                        })
                        .collect(Collectors.toList());
            }
            animationFrames.forEach(animationFrame -> animationFrame.setParent(this));
        }

        public List<AnimationFrameConfig> getAnimationFrames() {
            return animationFrames;
        }

        public void setAnimationFrames(List<AnimationFrameConfig> animationFrames) {
            this.animationFrames = animationFrames;
        }

        public double[][] getFrames() {
            return frames;
        }

        public void setFrames(double[][] frames) {
            this.frames = frames;
        }

        public boolean repeats() {
            return repeats;
        }

        public void setRepeats(boolean repeats) {
            this.repeats = repeats;
        }

        public int getRepeatCount() {
            return repeatCount;
        }

        public void setRepeatCount(int repeatCount) {
            this.repeatCount = repeatCount;
        }
    }

    public static class AnimationFrameConfig extends SpriteSheetConfig {
        private int u;
        private int v;
        private double duration;

        public int getU() {
            return u;
        }

        public void setU(int u) {
            this.u = u;
        }

        public int getV() {
            return v;
        }

        public void setV(int v) {
            this.v = v;
        }

        public double getDuration() {
            return duration;
        }

        public void setDuration(double duration) {
            this.duration = duration;
        }
    }

}
