package com.mush.game.utils.sprite;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class SpriteSheet {
    private final BufferedImage image;
    private int cellWidth;
    private int cellHeight;
    private int xOffset;
    private int yOffset;

    public SpriteSheet(String fileName) throws IOException {
        image = ImageIO.read(new File(fileName));
        setCellSize(16, 16).setOffset(0, 0);
    }

    public SpriteSheet setCellSize(int w, int h) {
        cellWidth = w;
        cellHeight = h;
        return this;
    }

    public SpriteSheet setOffset(int x, int y) {
        xOffset = x;
        yOffset = y;
        return this;
    }

    public SpriteSheet setOffsetInCells(int u, int v) {
        return setOffset(u * cellWidth, v * cellHeight);
    }

    public BufferedImage getCell(int u, int v) {
        BufferedImage cell = new BufferedImage(cellWidth, cellHeight, image.getType());
        Graphics2D g = cell.createGraphics();
        g.drawImage(image, -u * cellWidth - xOffset, -v * cellHeight - yOffset, null);
        g.dispose();
        return cell;
    }
}
