package com.mush.game.utils.sprite;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SpriteFactory {

    private final Logger logger = Logger.getLogger(this.getClass().getSimpleName());

    private SpriteFactoryConfiguration config;
    private Map<String, SpriteDefinition> spriteTypeDefinitionMap;
    private Map<String, SpriteSheet> spriteSheetMap;

    public SpriteFactory(SpriteFactoryConfiguration configuration) {
        setConfig(configuration);
    }

    public SpriteFactory(String jsonFileName) throws IOException {
        logger.log(Level.INFO, "Loading sprites from [{0}]", jsonFileName);
        setConfig(loadJson(jsonFileName));
    }

    private void setConfig(SpriteFactoryConfiguration configuration) {
        config = configuration;
        config.connect();
        spriteTypeDefinitionMap = new HashMap<>();
        spriteSheetMap = new HashMap<>();
    }

    private SpriteFactoryConfiguration loadJson(String fileName) throws IOException {
        File jsonFile = new File(fileName);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        SpriteFactoryConfiguration configuration = mapper.readValue(jsonFile, SpriteFactoryConfiguration.class);
        if (configuration.getSpriteTypes() == null) {
            configuration.setSpriteTypes(Collections.emptyMap());
        }
        return configuration;
    }

    public SpriteSheet getSpriteSheet(String spriteSheetFileName) {
        if (!spriteSheetMap.containsKey(spriteSheetFileName)) {
            spriteSheetMap.put(spriteSheetFileName, createSpriteSheet(spriteSheetFileName));
        }
        return spriteSheetMap.get(spriteSheetFileName);
    }

    private SpriteSheet createSpriteSheet(String spriteSheetFileName) {
        try {
            return new SpriteSheet(spriteSheetFileName);
        } catch (IOException e) {
            // Yes, cause null pointer exception
            return null;
        }
    }

    public SpriteDefinition getSpriteDefinition(String spriteType) {
        if (!spriteTypeDefinitionMap.containsKey(spriteType)) {
            spriteTypeDefinitionMap.put(spriteType, createSpriteDefinition(spriteType));
        }
        return spriteTypeDefinitionMap.get(spriteType);
    }

    private SpriteDefinition createSpriteDefinition(String spriteType) {
        SpriteDefinition definition = new SpriteDefinition(spriteType);

        SpriteFactoryConfiguration.SpriteTypeConfig typeConfig = config.getSpriteTypes().get(spriteType);

        typeConfig.getSpriteStates().forEach((state, stateConfig) -> {
            FrameAnimation animation = new FrameAnimation();

            stateConfig.getAnimationFrames().forEach(frameConfig -> {
                BufferedImage frameImage = getSpriteSheet(frameConfig.getSpriteSheet())
                        .setCellSize(frameConfig.getCellWidth(), frameConfig.getCellHeight())
                        .setOffsetInCells(frameConfig.getuOffset(), frameConfig.getvOffset())
                        .getCell(frameConfig.getU(), frameConfig.getV());

                SpriteFrame frame = new SpriteFrame(frameImage, frameConfig.getxCenter(), frameConfig.getyCenter());

                animation.addFrame(frame, frameConfig.getDuration());
            });

            animation.setRepeats(stateConfig.repeats());
            animation.setRepeatCount(stateConfig.getRepeatCount());

            definition.setAnimationForState(state, animation);
        });

        return definition;
    }
}
