package com.mush.game.utils.sprite;

import java.awt.image.BufferedImage;

public class SpriteFrame {

    private final BufferedImage frameImage;
    private final int frameCenterX;
    private final int frameCenterY;

    public SpriteFrame(BufferedImage frameImage, int frameCenterX, int frameCenterY) {
        this.frameImage = frameImage;
        this.frameCenterX = frameCenterX;
        this.frameCenterY = frameCenterY;
    }

    public BufferedImage getFrameImage() {
        return frameImage;
    }

    public int getFrameCenterX() {
        return frameCenterX;
    }

    public int getFrameCenterY() {
        return frameCenterY;
    }

    public int getWidth() {
        return frameImage.getWidth();
    }

    public int getHeight() {
        return frameImage.getHeight();
    }
}
