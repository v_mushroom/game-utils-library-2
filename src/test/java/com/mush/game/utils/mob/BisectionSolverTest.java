package com.mush.game.utils.mob;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BisectionSolverTest {

    @Test
    public void test() {
        assertEquals(0.0, BisectionSolver.solve(t -> true, step -> true), 0);
        assertEquals(0.55, BisectionSolver.solve(t -> t > 0.55, step -> step > 0.001), 0.01);
        assertEquals(0.2, BisectionSolver.solve(t -> t > 0.2, step -> step > 0.001), 0.01);
        assertEquals(0.9, BisectionSolver.solve(t -> t > 0.9, step -> step > 0.001), 0.01);
        assertEquals(0.0, BisectionSolver.solve(t -> t < 0.55, step -> step > 0.001), 0);
    }
}