package com.mush.game.utils.mob;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class CommutativeValuePairTest /*extends TestCase*/ {

    @Test
    public void test() {
        CommutativeValuePair collision1 = new CommutativeValuePair(1, 2);
        CommutativeValuePair collision2 = new CommutativeValuePair(2, 1);
        CommutativeValuePair collision3 = new CommutativeValuePair(1, 3);
        CommutativeValuePair collision4 = new CommutativeValuePair(2, 3);
        CommutativeValuePair collision5 = new CommutativeValuePair(1, 2);

        assertEquals(collision1, collision2);
        assertEquals(collision1, collision5);
        assertEquals(collision2, collision1);
        assertEquals(collision2, collision5);

        assertNotEquals(collision1, collision3);
        assertNotEquals(collision1, collision4);
        assertNotEquals(collision3, collision4);

        assertEquals(collision1.hashCode(), collision2.hashCode());
        assertEquals(collision1.hashCode(), collision5.hashCode());
        assertEquals(collision2.hashCode(), collision5.hashCode());

        assertNotEquals(collision1.hashCode(), collision3.hashCode());
        assertNotEquals(collision1.hashCode(), collision4.hashCode());
        assertNotEquals(collision3.hashCode(), collision4.hashCode());
    }

}